// BUILD Version: 0001

/// Copyright (c) 2016 Morepork Studios. All rights reserved.
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.

/// Module Name:
//	 CallbackHandler.h

/// Description:
//	 Holds a tree of callback functions.
//	 Functions are invoked when the OnCallback function is called.

/// Supported Callback Signatures:	
//	 OnText								<unsigned int, int>
//	 OnKey								<int, int, int, int>
//	 OnMouseButton						<int, int, int>
//	 OnFrameBufferSize					<int, int>
//	 OnIconify							<int>
//	 OnCursorEnter						<int>
//	 OnMouseScroll						<double, double>
//	 OnCursorPos						<double, double>
//	 OnFiledrop							<int, const char**>

/// Note:
//	 It is possible to enable additional callback signatures
//   by changing UNSUPPORTED_CALLBACK_SIGNATURES flag to <true>

/// Compiler - preprocessor work
#ifndef __CALLBACK_HANDLER_MEAPI__
#define __CALLBACK_HANDLER_MEAPI__

/// CALLBACK_HANDLER_DLL 
// Must be defined by applications that are linking against the DLL version of the library. 
/// _CALLBACK_HANDLER_BUILD_DLL 
// Defined by the CALLBACK_HANDLER configuration header when compiling the DLL version of the library.
#if defined(CALLBACK_HANDLER_DLL) && defined(_CALLBACK_HANDLER_BUILD_DLL)
#error "You may not have both CALLBACK_HANDLER_DLL and _CALLBACK_HANDLER_BUILD_DLL defined"
#endif

/// Building CallbackHandler as a DLL
#if (defined(_WIN32) || defined(_WIN64)) && defined(_CALLBACK_HANDLER_BUILD_DLL)
#define CALLBACK_HANDLER_MEAPI __declspec(dllexport) 
/// Calling CallbackHandler as a DLL
#elif (defined(_WIN32) || defined(_WIN64)) && defined(CALLBACK_HANDLER_DLL)
#define CALLBACK_HANDLER_MEAPI __declspec(dllimport) 
/// Building or calling CallbackHandler as a static library
#else
#define CALLBACK_HANDLER_MEAPI
#endif

/// CallbackHandler Dependancy
#ifndef _FUNCTIONAL_
#include <functional>
#endif 

using namespace std;
///
template<typename... Args> struct CallbackHandler
{		
	/// Call to initialize the handler.
	// @pre_condition: No memory allocated to store callback functions.
	// @post_condition: Creates data structure to store callback functions and allocates associated memory.
	// @thread_safety: Can be called from any thread.
	// @params: void
	// @return: false on intialization fail.
	CALLBACK_HANDLER_MEAPI static bool Init();

	///	Call to destroy the handler.
	// @thread_safety: Can be run from any thread.
	// @pre_condition: Contains functions that will be called on an event.
	// @post_condition: Removes all contained functions and frees associated memory.
	// @params: void
	// @return: void
	CALLBACK_HANDLER_MEAPI static void Destroy();

	/// Call to execute all stored callbacks with event data as callback arguments.
	// @thread_safety: Can be run from any thread.
	// @pre_condition: Stored functions waiting for callback event.
	// @post_condition: Stored callbacks will be executed and passed argument data.
	// @params: Args... Can accept multiple callback argument signatures(Refer: .h(30)).
	// @return: void
	CALLBACK_HANDLER_MEAPI static void OnCallbackEvent(Args... args);
	
	/// Call to add a ref to a callback function.
	// @thread_safety: Can be run from any thread.
	// @pre_condition: Callback count >= 0.
	// @post_condition: Callback count = callback count + 1.
	// @params: function<void(Args...)>& Accepts a function ref with multiple callback argument signatures(Refer: .h(30)).
	// @return: void
	CALLBACK_HANDLER_MEAPI static void Add(function<void(Args...)>& callback);

	/// Call to remove a ref to a callback function.
	// @thread_safety: Can be run from any thread.
	// @pre_condition: NC.
	// @post_condition: Callback count = callback count - 1.
	// @params:  function<void(Args...)>& Accepts a function ref with multiple callback argument signatures(Refer: .h(30)).
	// @return: void
	CALLBACK_HANDLER_MEAPI static void Remove(function<void(Args...)>& callback);

	/// Call to clear all callbacks.
	// @thread_safety: Can be run from any thread.
	// @pre_condition: Count().
	// @post_condition: Count() == 0.
	// @params: void.
	// @return: void
	CALLBACK_HANDLER_MEAPI static void Clear();

	/// Call to get number of stored callbacks.
	// @thread_safety: Can be run from any thread.
	// @pre_condition: NC.
	// @post_condition: NC.
	// @params: void.
	// @return: size_t The number of stored callbacks
	CALLBACK_HANDLER_MEAPI static size_t Count();
};	

/// Supported Argument Signatures
#define SignatureOnText											unsigned int, int
#define SignatureOnKey											int, int, int, int
#define SignatureOnMouseButton									int, int, int
#define SignatureOnFrameBufferSize								int, int
#define SignatureOnIconify										int
#define SignatureOnCursorEnter									int
#define SignatureOnMouseScroll									double, double
#define SignatureOnCursorPos									double, double
#define SignatureOnFiledrop										int, const char**

/// Supported CallbackHandlers
typedef CallbackHandler<SignatureOnText>						HandlerOnText;
typedef CallbackHandler<SignatureOnKey>							HandlerOnKey;
typedef CallbackHandler<SignatureOnMouseButton>					HandlerOnMouseButton;
typedef CallbackHandler<SignatureOnFrameBufferSize>				HandlerOnFrameBufferSize;
typedef CallbackHandler<SignatureOnIconify>						HandlerOnIconify;
typedef CallbackHandler<SignatureOnCursorEnter>					HandlerOnCursorEnter;
typedef CallbackHandler<SignatureOnMouseScroll>					HandlerOnMouseScroll;
typedef CallbackHandler<SignatureOnCursorPos>					HandlerOnCursorPos;
typedef CallbackHandler<SignatureOnFiledrop>					HandlerOnFiledrop;

/// Supported Callbacks
typedef function<void(SignatureOnText)>							CallbackOnText;
typedef function<void(SignatureOnKey)>							CallbackOnKey;
typedef function<void(SignatureOnMouseButton)>					CallbackOnMouseButton;
typedef function<void(SignatureOnFrameBufferSize)>				CallbackOnFrameBufferSize;
typedef function<void(SignatureOnIconify)>						CallbackOnIconify;
typedef function<void(SignatureOnCursorEnter)>					CallbackOnCursorEnter;
typedef function<void(SignatureOnMouseScroll)>					CallbackOnMouseScroll;
typedef function<void(SignatureOnCursorPos)>					CallbackOnCursorPos;
typedef function<void(SignatureOnFiledrop)>						CallbackOnFiledrop;

///
#define UNSUPPORTED_CALLBACK_SIGNATURES false
#if (UNSUPPORTED_CALLBACK_SIGNATURES)

/// Visual Studio throws warning when templates are defined in header
#pragma warning( disable : 4661 )

/// Note:
//  If handlers of NON supported types are required add them below..

/// Example 1:
//  template class CallbackHandlers< CustomObject* >;

/// Example 2:
//  template class CallbackHandlers< int >;

#endif /// UNSUPPORTED_CALLBACK_SIGNATURES

#endif //__CALLBACK_HANDLER_MEAPI__

/// Copyright (c) 2016 Morepork Studios. All rights reserved.
//  Author: Adam Charlton BiT(Bachelor of Information Technology) ... rNdm<rndm.baldr@gmail.com>