# Callback Handler #

A callback handler API, designed to work along side such libraries as GLFW.

The use case / spec is to reduce the complexity for handling callback events across the global scope of a single project.

* * *

~~~~

// BUILD Version: 0001

/// Copyright (c) 2016 Morepork Studios. All rights reserved.
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would
//    be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.

/// Module Name:
//	 CallbackHandler.h

/// Description:
//	 Holds a tree of callback functions.
//	 Functions are invoked when the OnCallbackEvent function is called.

/// Supported Callback Signatures:	
//	 OnText								<unsigned int, int>
//	 OnKey								<int, int, int, int>
//	 OnMouseButton						<int, int, int>
//	 OnFrameBufferSize					<int, int>
//	 OnIconify							<int>
//	 OnCursorEnter						<int>
//	 OnMouseScroll						<double, double>
//	 OnCursorPos						<double, double>
//	 OnFiledrop							<int, const char**>

/// Note:
//	 It is possible to enable additional callback signatures
//   by changing UNSUPPORTED_CALLBACK_SIGNATURES flag to <true>


~~~~

### API usage ###

~~~~

#include <CallbackHandler.h>

void OnKey(int key, int scancode, int action, int mods)
{
   // Handle event
}

void main(void)
{
   CallbackHandler<int, int, int, int> onKeyHandler;
   
   if(!onKeyHandler.Init())
      return 0;

   // A typical callback use case
   using namespace placeholders;
   auto callback = std::bind(&OnKey, _1, _2, _3, _4);
   onKeyHandler.Add(callback);
   
   {   
       // A dummy event this can be called from APIs such as GLFW
       // The resulting arguments will be passed to ALL callbacks stored in the handler
       onKeyHandler.OnCallbackEvent(0, 1, 2, 3);
   }

   // Remove the callback from the handler
   onKeyHandler.Remove(callback);

   // Cleanup the handler
   onKeyHandler.Destroy();

   return 0;
}

~~~~

### Alternative API usage ###

~~~~

#include <CallbackHandler.h>

class DummyClass
{
public:
   void Init();
   void OnKey(int key, int scancode, int action, int mods);
}

void DummyClass::Init()
{   
   // A typical callback use case
   using namespace placeholders;
   auto callback = std::bind(&DummyClass::OnKey, this, _1, _2, _3, _4);

   CallbackHandler<int, int, int, int>::Add(callback);
}

void DummyClass::OnKey(int key, int scancode, int action, int mods)
{
   // Handle event
}

void main(void)
{
   // Initialize the handler
   CallbackHandler<int, int, int, int>::Init();
        
   DummyClass* dummyClass = new DummyClass();
   dummyClass->Init();

   {   
       // A dummy event this can be called from APIs such as GLFW
       // The resulting arguments will be passed to ALL callbacks stored in the handler
       CallbackHandler<int, int, int, int>::OnCallbackEvent(0, 1, 2, 3);
   }
   
   // Cleanup the handler
   CallbackHandler<int, int, int, int>::Destroy();

   // Note: The handler is destroyed BEFORE the instantiated class
   delete dummyClass;

   return 0;
}

~~~~

### Contact ###

* Adam Charlton BiT - rNdm:  <rndm.baldr@gmail.com>